{
	// Place your global snippets here. Each snippet is defined under a snippet name and has a scope, prefix, body and 
	// description. Add comma separated ids of the languages where the snippet is applicable in the scope field. If scope 
	// is left empty or omitted, the snippet gets applied to all languages. The prefix is what is 
	// used to trigger the snippet and the body will be expanded and inserted. Possible variables are: 
	// $1, $2 for tab stops, $0 for the final cursor position, and ${1:label}, ${2:another} for placeholders. 
	// Placeholders with the same ids are connected.
	// Example:
	// "Print to console": {
	// 	"scope": "javascript,typescript",
	// 	"prefix": "log",
	// 	"body": [
	// 		"console.log('$1');",
	// 		"$2"
	// 	],
	// 	"description": "Log output to console"
	// }

	//Vanilla Roblox
	"Roblox Service": {
		"prefix": ["service"],
		"body": ["local ${0:Name}Service = game:GetService(\"${0:Name}Service\")"],
		"description": "Roblox Service"
	},
	"Roblox Class": {
		"prefix": ["class"],
		"body": [
			"local ${0:$TM_FILENAME_BASE} = {}",
			"${0:$TM_FILENAME_BASE}.__index = ${0:$TM_FILENAME_BASE}",
			"",
			"",
			"function ${0:$TM_FILENAME_BASE}.new()",
			"\tlocal self = setmetatable({}, ${0:$TM_FILENAME_BASE})",
			"\treturn self",
			"end",
			"",
			"",
			"function ${0:$TM_FILENAME_BASE}:Destroy()",
			"\t",
			"end",
			"",
			"",
			"return ${0:$TM_FILENAME_BASE}",
			""
		],
		"description": "Lua Class"
	},
	"Roblox Module": {
		"prefix": ["module"],
		"body": [
			"local ${0:$TM_FILENAME_BASE} = {}",
			"",
			"return ${0:$TM_FILENAME_BASE}"
		],
		"description": "Create basic Module boiler plate"
	},

	//Knit
	"Knit": {
		"prefix": ["knit"],
		"body": ["local Knit = require(ReplicatedStorage.Packages.Knit)"],
		"description": "Require the Knit module"
	},
	"Knit Service": {
		"prefix": ["knitservice"],
		"body": [
			"--Services",
			"local ReplicatedStorage = game:GetService(\"ReplicatedStorage\")",
			"",
			"local Knit = require(ReplicatedStorage.Packages.Knit)",
			"",
			"local ${0:$TM_FILENAME_BASE} = Knit.CreateService {",
			"\tName = \"${0:$TM_FILENAME_BASE}\",",
			"\tClient = {},",
			"}",
			"",
			"",
			"function ${0:$TM_FILENAME_BASE}:KnitStart()",
			"\t",
			"end",
			"",
			"",
			"function ${0:$TM_FILENAME_BASE}:KnitInit()",
			"\t",
			"end",
			"",
			"",
			"return ${0:$TM_FILENAME_BASE}",
			""
		],
		"description": "Knit Service template"
	},
	"Knit Controller": {
		"prefix": ["knitcontroller"],
		"body": [
			"--Services",
			"local ReplicatedStorage = game:GetService(\"ReplicatedStorage\")",
			"",
			"local Knit = require(ReplicatedStorage.Packages.Knit)",
			"",
			"local ${0:$TM_FILENAME_BASE} = Knit.CreateController { Name = \"${0:$TM_FILENAME_BASE}\" }",
			"",
			"",
			"function ${0:$TM_FILENAME_BASE}:KnitStart()",
			"\t",
			"end",
			"",
			"",
			"function ${0:$TM_FILENAME_BASE}:KnitInit()",
			"\t",
			"end",
			"",
			"",
			"return ${0:$TM_FILENAME_BASE}",
			""
		],
		"description": "Knit Controller template"
	},
	"Knit Require": {
		"prefix": ["knitrequire"],
		"body": ["local ${1:Name} = require(Knit.${2:Util}.${1:Name})"],
		"description": "Knit Require template"
	},

	//Roact
	"Roact": {
		"prefix": ["roact"],
		"body": [
			"--Services",
			"local ReplicatedStorage = game:GetService(\"ReplicatedStorage\")",
			"local Roact = require(ReplicatedStorage.Roact)",
		],
		"description": "Define the Roact module and ReplicatedStorage"
	},

	//Moonwave
	"Moonwave Class": {
		"prefix": ["moonclass"],
		"body": [
			"--[=[",
			"\t@class ${0:$TM_FILENAME_BASE}",
			"]=]",
			""
		],
		"description": "Start moonwave class comment syntax"
	},
	"Moonwave Method": {
		"prefix": ["moonmethod"],
		"body": [
			"--[=[",
			"@within $TM_FILENAME_BASE",
			"@method $1",
			"@since $2",
			"",
			"$3",
			"]=]"
		],
		"description": "Start moonwave comment syntax"
	},
	"Moonwave Parameter": {
		"prefix": ["moonparam"],
		"body": [
			"@param $1 $2 -- $3"
		],
		"description": "Start parameter syntax"
	},
	// "Moonwave Property": {
	// 	"prefix": ["moonprop"],
	// 	"body": [
	// 		"--[=[",
	// 		"@prop ${1:self.}$2 $3",
	// 		"@within $TM_FILENAME_BASE",
	// 		"]=]",
	// 	],
	// 	"description": "Start property syntax"
	// },

	//Combinations
	"Main Service": {
		"prefix": ["mainservice"],
		"body": [
			"--[=[",
			"\t@class $TM_FILENAME_BASE",
			"\t$3",
			"\t### ⏲Release Version:",
			"\t$2",
			"\t### 📃Description:",
			"\t$1",
			"]=]",
			"",
			"--Roblox Services",
			"local ReplicatedStorage = game:GetService(\"ReplicatedStorage\")",
			"",
			"local Knit = require(ReplicatedStorage.Packages.Knit)",
			"",
			"local $TM_FILENAME_BASE = Knit.CreateService {",
			"\tName = \"$TM_FILENAME_BASE\",",
			"\tClient = {},",
			"}",
			"",
			"--[=[",
			"@within $TM_FILENAME_BASE",
			"@method KnitStart",
			"@since $2",
			"Knit will call KnitStart after all services have been initialized",
			"",
			"]=]",
			"function $TM_FILENAME_BASE:KnitStart()",
			"\t",
			"end",
			"",
			"--[=[",
			"@within $TM_FILENAME_BASE",
			"@method KnitInit",
			"@since $2",
			"Knit will call KnitInit when Knit is first started",
			"",
			"]=]",
			"function $TM_FILENAME_BASE:KnitInit()",
			"\t",
			"end",
			"",
			"",
			"return $TM_FILENAME_BASE",
			""
		],
		"description": "Start main service syntax"
	},
	"Main Controller": {
		"prefix": ["maincontroller"],
		"body": [
			"--[=[",
			"\t@class $TM_FILENAME_BASE",
			"\t$3",
			"\t### ⏲Release Version:",
			"\t$2",
			"\t### 📃Description:",
			"\t$1",
			"",
			"]=]",
			"",
			"--Roblox Services",
			"local ReplicatedStorage = game:GetService(\"ReplicatedStorage\")",
			"",
			"local Knit = require(ReplicatedStorage.Packages.Knit)",
			"",
			"local $TM_FILENAME_BASE = Knit.CreateController { Name = \"$TM_FILENAME_BASE\" }",
			"",
			"--[=[",
			"@within $TM_FILENAME_BASE",
			"@method KnitStart",
			"@since $2",
			"Knit will call KnitStart after all services have been initialized",
			"",
			"]=]",
			"function $TM_FILENAME_BASE:KnitStart()",
			"\t",
			"end",
			"",
			"--[=[",
			"@within $TM_FILENAME_BASE",
			"@method KnitInit",
			"@since $2",
			"Knit will call KnitInit when Knit is first started",
			"",
			"]=]",
			"function $TM_FILENAME_BASE:KnitInit()",
			"\t",
			"end",
			"",
			"",
			"return $TM_FILENAME_BASE",
			""
		],
		"description": "Start main controller syntax"
	},

}

